//
//  AppDelegate.h
//  Memo
//
//  Created by teruakinakayama on 2013/10/04.
//  Copyright (c) 2013年 teruakinakayama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
